const jwt = require('jsonwebtoken');

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    return jwt.sign(data, process.env.TOKEN_SECRET, { expiresIn: '86400s' }); // 1 day
};

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;

    if (typeof token !== 'undefined') {
        console.log(token);
        token = token.slice(7, token.length);
        return jwt.verify(token, process.env.TOKEN_SECRET, (err, data) => {
            if (err) {
                return res.send({
                    auth: 'Token is not valid.'
                });
            }
            else {
                next()
            };
        });
    }
    else {
        return null;
    };
};

module.exports.decode = (token) => {
    if (typeof token !== 'undefined') {
        token = token.slice(7, token.length);
    };

    return jwt.verify(token, process.env.TOKEN_SECRET, (err, data) => {
        if (err) {
            return null;
        }
        else {
            return jwt.decode(token, { complete: true }).payload;
        };
    });
};