const mongoose = require('mongoose');
const auth = require('../auth');
const Product = require('../models/Product');


module.exports.addProduct = (data) => {
    return Product.findOne({ productName: data.productName }).then(result => {
        if (data.isAdmin) {
            let newProduct = new Product({
                productName: data.product.productName,
                description: data.product.description,
                price: data.product.price
            });
            return newProduct.save().then((newProduct, err) => {
                if (err) {
                    return err;
                }
                return 'Product created successfully!';
            });
        };
        let message = Promise.resolve('User must be ADMIN to add a product.');
        return message.then(value => {
            return { value };
        });
    });
};

module.exports.getActiveProducts = (req, res, next) => {
    return Product.find({ isActive: true }).then(result => {
        return result;
    });
};

module.exports.getSpecificProduct = (productId) => {
    return Product.findById(productId).then(result => {
        return result;
    });
};

module.exports.updateProduct = (productId, newData) => {
    if (newData.isAdmin) {
        return Product.findByIdAndUpdate(productId, {
            productName: newData.product.productName,
            description: newData.product.description,
            price: newData.product.price
        }).then((updateProduct, err) => {
            if (err) {
                return err;
            }
            return updateProduct;
        });
    } else {
        let message = Promise.resolve('User must be ADMIN to update product.');
        return message.then(value => {
            return { value }
        });
    };
};

module.exports.archiveProduct = (productId, newStatus) => {
    if (newStatus.isAdmin) {
        return Product.findByIdAndUpdate(productId, {
            isActive: false // newStatus.product.isActive
        }).then((archiveProduct, err) => {
            if (err) {
                return err;
            }
            return 'Archive product successful!';
        });
    } else {
        let message = Promise.resolve('User must be ADMIN to archive a product.');
        return message.then(value => {
            return { value };
        });
    };
};