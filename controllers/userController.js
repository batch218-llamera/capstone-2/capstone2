const User = require('../models/User');
const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.registerUser = (reqBody) => {
    return User.find({ email: reqBody.email }).then(result => {
        if (result.length > 0) {
            return 'Email already exists!';
        }
        else {
            const newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                isAdmin: reqBody.isAdmin,
                password: bcrypt.hashSync(reqBody.password, 10),
                mobileNo: reqBody.mobileNo
            });

            return newUser.save().then((user, err) => {
                if (err) {
                    return false;
                }
                else {
                    return 'Registration Successful!';
                }
            });
        };
    });
};

module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return 'User not found.';
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect) {
                return { access: auth.createAccessToken(result) };
            } else {
                return 'Incorrect password.';
            };
        };
    });
};

module.exports.getUserDetails = (userId) => {
    return User.findById(userId).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        }
        else if (result) {
            result.password = "****";
            return result;
        }
        else {
            return 'User not found';
        };
    });
};

module.exports.getActiveProducts = (req, res, next) => {
    return Product.find({ isActive: true }).then(result => {
        return result;
    });
};

module.exports.setAsAdmin = (userId, newStatus) => {
    if (newStatus.isAdmin) {
        return User.findByIdAndUpdate(userId, {
            isAdmin: true
        }).then((updateStatus, err) => {
            if (err) {
                return err;
            }
            return updateStatus;
        });
    } else {
        let message = Promise.resolve('User must be ADMIN to set Admin status.');
        return message.then(value => {
            return value
        });
    };
};
