const express = require('express');
const Order = require('../models/Order');
const mongoose = require('mongoose');
const auth = require('../auth');
const Cart = require('../models/Cart');
const Product = require('../models/Product');

module.exports.createOrder = async (reqBody) => {
    const cartIds = Promise.all(reqBody.cart.map(async (cartItem) => {
        let newCartItem = new Cart({
            quantity: cartItem.quantity,
            productId: cartItem.productId
        });
        newCartItem = await newCartItem.save();
        return newCartItem._id;
    }));

    const cartIdsResolved = await cartIds;
    //console.log(cartIdsResolved);

    const totalPrices = await Promise.all(cartIdsResolved.map(async (cartItemId) => {
        const cartItem = await Cart.findById(cartItemId).populate('productId', 'price');
        const totalPrice = cartItem.productId.price * cartItem.quantity;
        return totalPrice;
    }));

    console.log(totalPrices);
    const totalPrice = totalPrices.reduce((a, b) => a + b, 0);

    const newOrder = await new Order({
        cart: cartIdsResolved,
        shippingAddress: reqBody.shippingAddress,
        city: reqBody.city,
        zip: reqBody.zip,
        country: reqBody.country,
        phone: reqBody.phone,
        status: reqBody.status,
        totalPrice: totalPrice,
        userId: reqBody.userId
    });

    return newOrder.save().then((newOrder, err) => {
        if (err) {
            return err;
        }
        return newOrder;
    });
};

module.exports.getAllOrders = (data) => {
    if (data.isAdmin) {
        return Order.find().populate('userId', 'firstName').populate('cart', 'quantity').sort({ 'dateOrdered': -1 }).then((result, error) => {
            if (error) {
                return error;
            }
            return result;
        });
    };
    let message = Promise.resolve('User must be ADMIN to change status.');
    return message.then((value) => {
        return value
    });
};

module.exports.getUserOrders = (userId) => {
    return Order.find({ userId: userId }).populate('userId', 'firstName').populate('cart', 'quantity').sort({ 'dateOrdered': -1 }).then(result => {
        if (result.length > 0) {
            return result;
        }
        return 'Orders not found';
    });
};
