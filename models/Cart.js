const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
    quantity: {
        type: Number,
        required: true
    },
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },
    subTotalPrice: {
        type: Number
    }
});

module.exports = mongoose.model('Cart', cartSchema);
