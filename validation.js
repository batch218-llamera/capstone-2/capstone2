const Joi = require('joi');

module.exports.register = Joi.object()
    .keys({
        firstName: Joi.string()
            .min(2)
            .max(30)
            .required(),
        lastName: Joi.string()
            .min(2)
            .max(30)
            .required(),
        email: Joi.string()
            .email()
            .min(6)
            .required(),
        password: Joi.string()
            .min(6)
            .required(),
        mobileNo: Joi.string()
            .min(6)
            .required(),
    });

module.exports.login = Joi.object()
    .keys({
        email: Joi.string()
            .email()
            .min(6)
            .required(),
        password: Joi.string()
            .min(6)
            .required()
    });