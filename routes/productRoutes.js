const express = require('express');
const router = express.Router();

const Product = require('../models/Product');
const productController = require('../controllers/productController');
const auth = require('../auth');
const routes = require('./userRoutes');


// add product
router.post('/add-product', auth.verify, (req, res) => {
    let newProduct = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productController.addProduct(newProduct).then(result => res.send(result));
});

// get active products
router.get('/active', (req, res) => {
    productController.getActiveProducts().then(result => res.send(result));
});

// get specific product
router.get('/:productId', (req, res) => {
    productController.getSpecificProduct(req.params.productId).then(result => res.send(result));
});

// update product details
router.patch('/update/:productId', auth.verify, (req, res) => {
    const newDetails = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productController.updateProduct(req.params.productId, newDetails).then(result => res.send(result));
});

//archive a product
router.patch('/archive/:productId', auth.verify, (req, res) => {
    const newStatus = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productController.archiveProduct(req.params.productId, newStatus).then(result => res.send(result));
});

module.exports = router;
