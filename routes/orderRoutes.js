const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../models/Order');
const orderController = require('../controllers/orderController');
const auth = require('../auth');

router.post('/checkout', (req, res) => {
    orderController.createOrder(req.body).then(result => res.send(result));
});

router.get('/', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    orderController.getAllOrders(data).then(result => res.send(result));
});

router.get('/my-orders', auth.verify, (req, res) => {
    const userId = auth.decode(req.headers.authorization).id
    orderController.getUserOrders(userId).then(result => res.send(result));
})

module.exports = router;