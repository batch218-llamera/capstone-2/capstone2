const express = require('express');
const router = express.Router();

const User = require('../models/User');
const userController = require('../controllers/userController');
const auth = require('../auth');
const validation = require('../validation')
const { validate } = require('../validationMiddleware')

router.post('/register', validate(validation.register), (req, res) => {
    userController.registerUser(req.body).then(result => res.send(result));
});

router.post('/login', validate(validation.login), (req, res) => {
    userController.loginUser(req.body).then(result => res.send(result));
});

router.get('/:userId/userDetails', auth.verify, (req, res) => {
    userController.getUserDetails(req.params.userId).then(result => res.send(result));
});

router.patch('/:userId/setAsAdmin', auth.verify, (req, res) => {
    const newStatus = {
        user: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };
    userController.setAsAdmin(req.params.userId, newStatus).then(result => res.send(result));
});


module.exports = router;
